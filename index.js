const express = require("express")
const path = require('path')
const port = 3073
const app = express()
const bodyParser = require("body-parser")
const cors = require('cors')

app.use( (req,res, next)=>{
    next()
} )

app.use(bodyParser.json())
app.use(cors())
app.use( express.static(path.join( __dirname, "dist" )) )
app.use("/api", require("./server/router.js"))
app.use("/images", express.static(path.join(__dirname, "server", "temporary")))
app.get("*", (req, res)=>{ res.sendFile( path.join(__dirname, 'dist', 'index.html') ) })

app.listen( port, ()=>{
    console.log(`static server on http://localhost:${port}`)
} )