const path = require("path")
const CanvasEditor = require("../dist/assets/js/canvas-editor.js")
const fs = require('fs')

module.exports.createImageFromCanvas = async function(req, res){
    try{
        if (req?.body?.elements?.length){
            let elements = req.body.elements
            let CE = new CanvasEditor()
            CE.mount({ dimension: { width:1366, height: 768 } })
            for (let i=0; i<elements.length; i++){
                CE.addElement(elements[i])
            }
            let buffer = await CE.getBuffer()
            let imageBase64 = Buffer.from(buffer, 'base66')
            let fileName = "test.png"
            let filePath = path.join(__dirname, 'temporary', fileName)
            fs.writeFile( filePath, imageBase64, (err)=>{
                let responsrObj = {success: true, message: "Image generated", fileName: fileName}
                if (err){
                    console.log(err)
                    responsrObj.success = false
                    responsrObj.fileName = ""
                    responsrObj.message = "Failed to generate Image"
                }
                res.status(200).json(responsrObj)
            } )

        }
        
    }catch(err){
        console.log(err)
        res.status(500).json()
    }
}