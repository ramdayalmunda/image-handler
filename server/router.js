const controller = require("./controller");

const router = require("express").Router();

router.post("/create-canvas-image", controller.createImageFromCanvas)


router.use("*", (req, res) => { res.status(404).json() })

module.exports = router