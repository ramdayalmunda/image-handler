import api from "./api.js"
var elements = [
    {
        type: "text", // type can be// text // block // overlay // image
        text: "wrd1 ord2 wod3 wor4 AppearsLineNoTwo 2MightFit ThirdLineIsHere",
        dimension: {
            width: 680,
            height: 150
        },
        position: {
            left: 236,
            top: 50,
        },
        font: {
            size: 100,
            family: "Poppins",
            familyUrl: "/assets/fonts/Poppins-Regular.ttf",
            color: "#ff1245"
        },
        transform: {
            xFlip: false,
            yFlip: false,
            angle: 0
        },
        format: {
            backgroundColor: "#000000af",
            borderRadius: 7,
            borderWidth: 0,
            borderColor: "#4faf1f"
        }
    }
]

var fontFamily = [
    { name: "Poppins", url: "/assets/fonts/Poppins-Regular.ttf" },
    { name: "Calibri", url: "/assets/fonts/calibri-regular.ttf" },
    { name: "Work Sans", url: "/assets/fonts/WorkSans-Regular.ttf" },
    { name: "Kenia", url: "/assets/fonts/Kenia-Regular.ttf" },
    { name: "Lato", url: "/assets/fonts/Lato-Regular.ttf" },
    { name: "Lora", url: "/assets/fonts/Lora-Regular.ttf" },
    { name: "Macondo", url: "/assets/fonts/Macondo-Regular.ttf" },
    { name: "Merriweather", url: "/assets/fonts/Merriweather-Regular.ttf" },
    { name: "Montserrat", url: "/assets/fonts/Montserrat-VariableFont_wght.ttf" },
    { name: "Open Sans", url: "/assets/fonts/OpenSans-VariableFont_wdth,wght.ttf" },
    { name: "Rubic", url: "/assets/fonts/Rubik-VariableFont_wght.ttf" },
    { name: "Ubuntu", url: "/assets/fonts/Ubuntu-Regular.ttf" },
    { name: "Indie Flower", url: "/assets/fonts/IndieFlower-Regular.ttf" },
    { name: "Architects Daughter", url: "/assets/fonts/ArchitectsDaughter-Regular.ttf" },
    { name: "Caveat", url: "/assets/fonts/Caveat-VariableFont_wght.ttf" },
    { name: "Dancing Script", url: "/assets/fonts/DancingScript-VariableFont_wght.ttf" },
    { name: "Edu TAS Beginner", url: "/assets/fonts/EduTASBeginner-VariableFont_wght.ttf" },
]
var CE;
var toolbar;
var toolData = {
    fontFamily: "Calibri",
    fontSize: 24,
    bold: false,
    italic: false,
    fontColor: "#00ff00",
    borderWidth: 10,
    borderColor: "#ff6699",
    backgroundColor: "#ffffff7f"
}

var selectedElem = document.querySelector('#custom-element-select')
var addElemBtn = document.querySelector('#add-elem-btn')
var extractBtn = document.querySelector('#extract-btn')
let hideToolbarBtn = document.querySelector('#hide-toolbar-btn')
var inputField;
!(function () {``
    CE = new CanvasEditor()
    CE.mount({
        containerId: "container",
        dimension: {
            width: 1366,
            height: 768,
        }
    })

    elements.forEach(async item => {
        await loadFont({ name: item.font.family, url: item.font.familyUrl })
        CE.reRenderCanvas()
    })

    CE.addElement(elements[0])


    // custom events
    CE.getCanvas().addEventListener('dblclick', (e) => {
        let selectedElem = CE.getSelectedItemData()
        if (selectedElem) {
            toolbar.style.display = "block"
            setToolbarData(selectedElem)
        }
    })

    toolbar = document.getElementById('CE-custom-toolbar')
    toolData.fontFamily = document.getElementById('CE-font-family')
    toolData.fontFamily.innerHTML = ""
    fontFamily.forEach(item => {
        let option = document.createElement('option');
        option.append(item.name)
        option.setAttribute('value', JSON.stringify(item))
        toolData.fontFamily.append(option)

        !(async function () {
            await loadFont(item)
            option.style.fontFamily = `'${item.name}'`
        })()
    })
    toolData.fontFamily.addEventListener('change', changeFontFamily)
    toolData.bold = document.getElementById('CE-bold')
    toolData.italic = document.getElementById('CE-italic')
    inputField = document.getElementById('sample-font')




    async function changeText(e){
        console.log('newValue:', e.target.value)
        let se = await CE.getSelectedItemData()
        if (se?.type == 'text'){
            CE.modifySelected( { text: e.target.value } )
        }
    }
    inputField.addEventListener('input', changeText)
    function addElement(e) {
        let newObj = {
            type: "text", // type can be// text // block // overlay // image
            dimension: {
                width: 300,
                height: 300
            },
            position: {
                left: 400,
                top: 300,
            },
            font: {
                size: 100,
                family: "Poppins",
                familyUrl: "/assets/fonts/Poppins-Regular.ttf",
                color: "#ff1245"
            },
            transform: {
                xFlip: false,
                yFlip: false,
                angle: 0
            },
            format: {
                backgroundColor: "#00000000",
                borderRadius: 0,
                borderWidth: 0,
                borderColor: "#00000000"
            }
        }
        if (selectedElem.value == '/assets/img/arrow.svg') {
            newObj = { ...newObj,
                type: "image", // type can be// text // block // overlay // image
                image: {
                    src: selectedElem.value
                }
            }
            CE.addElement(newObj)
        }else if ( selectedElem.value == 'text' ){
            newObj = { ...newObj,
                type: 'text',
                text: "Hello World! ThisWordExceeds",
                dimension: { width: 650, height: 150 },
                position: { left: 50, top: 300 },
            }
            CE.addElement(newObj)
        }
    }
    addElemBtn.addEventListener('click', addElement)

    function extractData(){
        let elements = CE.getElements()
        console.log(elements)
        return elements
    }
    extractBtn.addEventListener('click', extractData)

    function hideToolbar(){
        toolbar.style.display = 'none'
    }
    hideToolbarBtn.addEventListener('click', hideToolbar)

})()

function setToolbarData(selectedElem) {
    toolData.fontFamily.value = JSON.stringify({ name: selectedElem.font.family, url: selectedElem.font.familyUrl })
}

async function changeFontFamily() {
    let selectedElem = CE.getSelectedItemData()
    if (selectedElem) {
        let selected = JSON.parse(toolData.fontFamily.value)
        await loadFont(selected)
        CE.modifySelected({ font: { family: selected.name, familyUrl: selected.url } })
        inputField.style.fontFamily = selected.name
    }
}

function loadFont(fontObj) {
    return new Promise(async function (res, rej) {
        try {
            if (!fontObj || !fontObj?.name || !fontObj.url) {
                res(false)
                return
            }
            if (!document.fonts.check(`16px ${fontObj.name}`)) {
                var customFont = new FontFace(fontObj.name, `url(${fontObj.url})`);
                customFont.load().then(function (font) {
                    document.fonts.add(font);
                    res(true)
                })
            } else res(true)
        } catch (err) {
            res(false)
        }

    })
}
let boldBox = document.getElementById('CE-bold')
boldBox.addEventListener('change', toggleBold)
function toggleBold(e) {
    let selectedElem = CE.getSelectedItemData()
    if (selectedElem) {
        modifySelected(selectedElem, { font: { bold: e.target.checked } })
    }
}

let italicBox = document.getElementById('CE-italic')
italicBox.addEventListener('change', toggleItalic)
function toggleItalic(e) {
    let selectedElem = CE.getSelectedItemData()
    if (selectedElem) {
        modifySelected(selectedElem, { font: { italic: e.target.checked } })
    }
}

function modifySelected(selectedElem, change) {

    let font = { ...selectedElem.font }
    if (change.font) font = { ...font, ...change.font }
    selectedElem.font = font
    CE.modifySelected(selectedElem)
}

async function downloadImage() {
    let payload = CE.getElements()
    let { data } = await api.post("/create-canvas-image", { elements: payload })
    if (data.success) window.open("/images/" + data.fileName)
}

let btnDownloadImage = document.getElementById('download-image-btn')
btnDownloadImage.addEventListener('click', downloadImage)