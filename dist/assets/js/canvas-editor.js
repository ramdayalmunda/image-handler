let isModule = (typeof module != 'undefined') ? true : false
var CanvasEditor = function () {
    if (isModule) var { createCanvas, Image: cImage } = require('canvas')
    var uniqueCounter = 1;
    var canvas;
    var container;
    var elements = []
    var handleElements = [
        { name: "left-middle", points: [{ x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }], oppositeIndex: 1, position: { left: -1, top: -1 }, horizontal: 1, vertical: 0 },
        { name: "right-middle", points: [{ x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }], oppositeIndex: 0, position: { left: -1, top: -1 }, horizontal: -1, vertical: 0 },
        { name: "rotate", points: [{ x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }], oppositeIndex: -1, position: { left: -1, top: -1 } },
        { name: "top-middle", points: [{ x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }], oppositeIndex: 4, position: { left: -1, top: -1 }, horizontal: 0, vertical: 1 },
        { name: "bottom-middle", points: [{ x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }], oppositeIndex: 3, position: { left: -1, top: -1 }, horizontal: 0, vertical: -1 },
        { name: "top-left", points: [{ x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }], oppositeIndex: 8, position: { left: -1, top: -1 } },
        { name: "top-right", points: [{ x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }], oppositeIndex: 7, position: { left: -1, top: -1 } },
        { name: "bottom-left", points: [{ x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }], oppositeIndex: 6, position: { left: -1, top: -1 } },
        { name: "bottom-right", points: [{ x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: -1 }], oppositeIndex: 5, position: { left: -1, top: -1 } },
    ]
    let images = []
    let assetsToLoad = 0;
    let callbackAfterLoading = null
    var configuration = { angleSnap: [0, 90, 180, 270, 360], angleSnapRange: 5, env: "dev" }
    var hoverElement = null;
    var selectedElement = null;
    var isAction = null; // set to 'dragging' // 'resizing' // 'rotating'
    var actionData = {
        clickCounter: 0,
    }
    var hoverHandle = null;
    var selectedHandle = null;
    var oppositeHandle = null;

    var constants = {
        handleLineWidth: 4,
        handleStrokeStyle: "#8bfff9",
        fillStyle: "#fff",
        handleWidth: 25,
    }
    var historyIndex = -1; // -1 indicates; no more undo possible
    var initialData = []; // the last undo option
    var history = []
    function init() {
        if (isModule) {
            canvas = new createCanvas(configuration.dimension.width, configuration.dimension.height)
        } else {
            canvas = document.createElement('canvas')
            canvas.setAttribute('id', `my-canvas-${new Date().getTime()}`);
            canvas.setAttribute('style', `position:absolute; top:0; left:0; width:100%;`)
            canvas.height = configuration.dimension.height
            canvas.width = configuration.dimension.width
            container.append(canvas)
            canvas.addEventListener('mousedown', mouseDown)
            canvas.addEventListener('mouseleave', mouseLeave)
            canvas.addEventListener('mouseup', mouseUp)
            canvas.addEventListener('mousemove', mouseMove)
            canvas.addEventListener('wheel', printMousePos)
        }
    }

    function mouseLeave(e) {
        if (actionData.actionType != 'dragging') {
            isAction = null;
            actionData.actionType = null
        }
    }


    function printMousePos(e) {
    }
    function getMousePos(evt) {
        var clx, cly
        if (evt.type == "touchstart" || evt.type == "touchmove") {
            clx = evt.touches[0].clientX;
            cly = evt.touches[0].clientY;
        } else {
            clx = evt.clientX;
            cly = evt.clientY;
        }
        var boundingRect = canvas.getBoundingClientRect();
        return {
            x: (clx - boundingRect.left) * canvas.width / canvas.clientWidth,
            y: (cly - boundingRect.top) * canvas.height / canvas.clientHeight
        };
    }

    function finalizeAction() {
        let changeObj = {}
        if (actionData.actionType == 'dragging') {
            selectedElement.position.top + (actionData.to.y - actionData.from.y)
            changeObj.position = {
                left: selectedElement.position.left + (actionData.to.x - actionData.from.x),
                top: selectedElement.position.top + (actionData.to.y - actionData.from.y)
            }
        }
        else if (actionData.actionType == 'resizing') {
            changeObj.position = {
                left: actionData.to.left,
                top: actionData.to.top
            }
            changeObj.dimension = {
                width: actionData.to.width,
                height: actionData.to.height,
            }
        }
        else if (actionData.actionType == 'rotating') {
            changeObj.transform = { angle: actionData.to.angle }
        }
        // else if (actionData.type == 'formatting') {
        //     selectedElement.font.family = actionData.to.family
        //     selectedElement.font.size = actionData.to.size
        //     selectedElement.font.color = actionData.to.color
        //     selectedElement.font.bold = actionData.to.boldy
        //     selectedElement.font.italic = actionData.to.italic
        //     changeObj.font = { ...selectedElement.font }
        //     selectedElement.format.backgroundColor = actionData.to.backgroundColor
        //     selectedElement.format.borderColor = actionData.to.borderColor
        //     selectedElement.format.borderWidth = actionData.to.borderWidth
        //     changeObj.format = { ...selectedElement.format }
        // }
        if (['dragging', 'resizing', 'rotating'].includes(actionData.actionType)) modifySelected(changeObj, { toRender: false, isHistory: false })
        delete actionData.from
        delete actionData.to
    }

    function mouseMove(e) {
        // Draw the quadrilateral
        let pos = getMousePos(e)
        if (isAction && selectedElement) {
            let oldCenter = { x: actionData.from.centerX, y: actionData.from.centerY }
            if (isAction == 'dragging') {
                actionData.to = pos
                actionData.actionType = isAction
            }
            else if (isAction == 'resizing') {
                actionData.actionType = isAction
                actionData.to = pos
                actionData.to.y = actionData.from.y

                pos = getMousePos(e)

                let newPoint = getPerpendicular(
                    { x: oppositeHandle.position.left, y: oppositeHandle.position.top },
                    { x: selectedHandle.position.left, y: selectedHandle.position.top },
                    pos,
                )
                if (selectedHandle.horizontal && selectedHandle.vertical) {
                } else {
                    let oppositeDistance = Math.sqrt(((oppositeHandle.position.left - newPoint.x) ** 2) + ((oppositeHandle.position.top - newPoint.y) ** 2))
                    let newCenter = { ...oldCenter }

                    if (selectedHandle.horizontal) {

                        newCenter.x = actionData.from.centerX + (selectedHandle.horizontal) * (selectedElement.dimension.width - oppositeDistance) / 2
                        let finalCenter = rotatePoint([newCenter.x, newCenter.y], [oldCenter.x, oldCenter.y], -selectedElement.transform.angle)

                        actionData.to.left = finalCenter.x - oppositeDistance / 2
                        actionData.to.top = finalCenter.y - selectedElement.dimension.height / 2
                        actionData.to.width = oppositeDistance
                        actionData.to.height = selectedElement.dimension.height
                    } else {

                        newCenter.y = actionData.from.centerY + (selectedHandle.vertical) * (selectedElement.dimension.height - oppositeDistance) / 2
                        let finalCenter = rotatePoint([newCenter.x, newCenter.y], [oldCenter.x, oldCenter.y], -selectedElement.transform.angle)

                        actionData.to.left = finalCenter.x - selectedElement.dimension.width / 2
                        actionData.to.top = finalCenter.y - oppositeDistance / 2
                        actionData.to.width = selectedElement.dimension.width
                        actionData.to.height = oppositeDistance
                    }
                }

            }
            else if (isAction == 'rotating') {
                let angle = Math.atan2((pos.y - oldCenter.y), (pos.x - oldCenter.x)) * 180 / Math.PI
                angle += 90; // oue all elemtents have off by 90 always
                angle = (angle % 360 + 360) % 360; // this is to resolve negative and >360 values
                if (configuration.angleSnap) {
                    for (let i = 0; i < configuration.angleSnap.length; i++) {
                        let snapAngle = configuration.angleSnap[i]
                        let abs = Math.abs(angle - snapAngle)
                        if (abs <= configuration.angleSnapRange) {
                            angle = snapAngle
                            break
                        }
                    }
                }
                actionData.to = { angle: angle }
                actionData.actionType = isAction
            }
            reRenderCanvas()
        } else {
            let found = false;
            // check handles
            for (let i = 0; i < handleElements.length; i++) {
                var points = handleElements[i].points
                let isInside = isPointInTriangle(pos, points[0], points[1], points[2]) || isPointInTriangle(pos, points[0], points[3], points[2])
                if (isInside) {
                    found = true;
                    hoverHandle = handleElements[i]
                    break;
                }
            }
            if (!found) {
                hoverHandle = null
                // then check element
                for (let i = elements.length - 1; i >= 0; i--) {
                    let points = elements[i].points
                    if (points?.length && points.length == 4) {
                        var isInside = isPointInTriangle(pos, points[0], points[1], points[2]) || isPointInTriangle(pos, points[0], points[3], points[2])
                        if (isInside) {
                            hoverElement = elements[i]
                            found = true
                            break;
                        }
                    }
                }
            }

            if (!found) hoverElement = null;
        }
    }
    function getPerpendicular(p1, p2, p3) {
        let slopeP1 = (p2.y - p1.y) / (p2.x - p1.x)
        let slopeP2 = -1 / slopeP1
        slopeP2 = Math.round(slopeP2 * 1000) / 1000
        let p4 = { x: NaN, y: NaN }
        if (slopeP1 == 0) {
            p4.x = p3.x
            p4.y = p1.y
        }
        else if (slopeP2 == 0) {
            p4.x = p1.x
            p4.y = p3.y
        }
        else {
            p4.x = (p3.y - p1.y + slopeP1 * p1.x - slopeP2 * p3.x) / (slopeP1 - slopeP2);
            p4.y = p1.y + slopeP1 * p4.x - slopeP1 * p1.x
        }
        return p4
    }

    function fillCanvas(color) {
        let ctx = canvas.getContext('2d')
        ctx.save()
        ctx.fillStyle = color;
        ctx.fillRect(0, 0, canvas.width, canvas.height)
        ctx.restore()
    }

    function reRenderCanvas() {
        let ctx = canvas.getContext('2d')
        ctx.save()
        ctx.clearRect(0, 0, canvas.width, canvas.height)

        // loop and check overlays and draw data
        for (let i = 0; i < elements.length; i++) {
            if (elements[i].type == 'overlay') {
                fillCanvas(elements[i].format.backgroundColor)
            }
        }
        for (let i = 0; i < elements.length; i++) {
            let tempElemData = elements[i]

            if (tempElemData.id == selectedElement?.id) {
                continue
            }

            renderElement(tempElemData)
        }

        // render the handles if selectedItem is Present
        if (selectedElement) {
            let tempElemData = selectedElement
            if (actionData.actionType == 'dragging') {
                tempElemData = JSON.parse(JSON.stringify(selectedElement))
                tempElemData.position.left += (actionData.to.x - actionData.from.x)
                tempElemData.position.top += (actionData.to.y - actionData.from.y)
            }
            if (actionData.actionType == 'resizing') {
                tempElemData = JSON.parse(JSON.stringify(selectedElement))
                tempElemData.position.left = actionData.to.left
                tempElemData.position.top = actionData.to.top
                tempElemData.dimension.width = actionData.to.width
                tempElemData.dimension.height = actionData.to.height
            }
            if (actionData.actionType == 'rotating') {
                tempElemData = JSON.parse(JSON.stringify(selectedElement))
                tempElemData.transform.angle = actionData.to.angle
            }

            // this is element
            renderElement(tempElemData)

            // this is border
            insertHandles({
                ...tempElemData,
                position: {
                    left: tempElemData.position.left,
                    top: tempElemData.position.top,
                },
                format: {
                    backgroundColor: "#00000000",
                    borderColor: "#6bffd9",
                    borderWidth: 2,
                }
            })
        }

        ctx.restore()
        if (assetsToLoad == 0 && callbackAfterLoading) {
            callbackAfterLoading()
        }
    }

    function mouseDown(e) {
        if (e.button == 2) {
            mouseUp(e)
            return
        }
        let pos = getMousePos(e)
        actionData.clickCounter++

        if (selectedElement) {

            actionData.from = pos
            actionData.from.left = selectedElement.position.left
            actionData.from.top = selectedElement.position.top
            actionData.from.centerX = selectedElement.position.left + selectedElement.dimension.width / 2
            actionData.from.centerY = selectedElement.position.top + selectedElement.dimension.height / 2
            actionData.from.height = selectedElement.dimension.height
            actionData.from.width = selectedElement.dimension.width
            actionData.from.angle = selectedElement.transform.angle
        }

        if (hoverHandle && selectedElement) {
            selectedHandle = hoverHandle
            oppositeHandle = handleElements[selectedHandle.oppositeIndex]
            isAction = selectedHandle.name == 'rotate' ? 'rotating' : 'resizing'
        } else {
            selectedHandle = null
            selectedElement = hoverElement
            if (selectedElement) {
                isAction = 'dragging'
                actionData.from = pos
            } else {
                isAction = null;
            }
        }

        reRenderCanvas()
    }
    function mouseUp(e) {
        if (isAction) {
            finalizeAction(actionData)
            actionData.actionType = null
            reRenderCanvas()
        }
        isAction = null;
        actionData.actionType = isAction

    }
    function isPointInTriangle(p0, p1, p2, p3) {
        // Calculate the area of the main triangle
        var area = 0.5 * (-p2.y * p3.x + p1.y * (-p2.x + p3.x) + p1.x * (p2.y - p3.y) + p2.x * p3.y);

        // Calculate the barycentric coordinates
        var s = (1 / (2 * area)) * (p1.y * p3.x - p1.x * p3.y + (p3.y - p1.y) * p0.x + (p1.x - p3.x) * p0.y);
        var t = (1 / (2 * area)) * (p1.x * p2.y - p1.y * p2.x + (p1.y - p2.y) * p0.x + (p2.x - p1.x) * p0.y);

        // Check if the point is inside the triangle
        return s > 0 && t > 0 && 1 - s - t > 0;
    }

    function rotatePoint(point, fixedPoint, angleDegrees) {
        // Convert angle from degrees to radians
        var angleRadians = -angleDegrees * Math.PI / 180;

        // Compute new coordinates
        var new_x = fixedPoint[0] + (point[0] - fixedPoint[0]) * Math.cos(angleRadians) - (point[1] - fixedPoint[1]) * Math.sin(angleRadians);
        var new_y = fixedPoint[1] + (point[0] - fixedPoint[0]) * Math.sin(angleRadians) + (point[1] - fixedPoint[1]) * Math.cos(angleRadians);

        // Return new coordinates
        return { x: new_x, y: new_y };
    }

    function insertText(elemData) {

        const ctx = canvas.getContext('2d');

        ctx.save()

        // Set the maximum width for the text
        const wrapWidth = elemData.dimension.width;

        // Define the text to be displayed
        const text = elemData.text;
        // // Rotate the canvas by 45 degrees
        ctx.font = `${elemData.font.italic ? 'italic ' : ''}${elemData.font.bold ? 'bold ' : ''}${elemData.font.size}px ${elemData.font.family}`;
        ctx.fillStyle = `${elemData.font.color}`;

        // Set the initial position for the text
        let x = 0;
        let y = 0;
        let lineHeight = elemData.font.lineHeight ? elemData.font.lineHeight : elemData.font.size;

        // Split the text into words
        const words = text.split(' ');
        let line = '';
        let linesArr = []
        for (let i = 0; i < words.length; i++) {
            let testWidth = ctx.measureText( line+words[i] ).width
            if (testWidth<wrapWidth){
                line += words[i] + ' '
            }else{
                let wordWidth = ctx.measureText( words[i] ).width
                if ( wordWidth<wrapWidth ){
                    linesArr.push( [ line, x, y ] )
                    line = words[i]+' '
                    y += lineHeight
                }else{
                    linesArr.push( [line, 0,y] )
                    let lineWidth = ctx.measureText(line).width
                    line = ""
                    let letters = ""
                    for( let l=0; l<words[i].length; l++ ){
                        let widthWithLetters = ctx.measureText( letters+words[i][l] ).width
                        if ( (widthWithLetters+lineWidth)<wrapWidth ){
                            letters += words[i][l]
                        }else{
                            linesArr.push( [letters, lineWidth, y] )
                            y += lineHeight
                            words.splice( i+1, 0, words[i].slice(letters.length) )
                            words[i] = letters
                            letters = ''
                            line = ""
                            break
                        }
                    }
                }
            }
        }
        linesArr.push([line, x, y])

        let oldCenter = [elemData.position.left + elemData.dimension.width / 2, elemData.position.top + elemData.dimension.height / 2]
        elemData.dimension.height = y + elemData.font.size + elemData.font.size / 3
        let newCenter = [elemData.position.left + elemData.dimension.width / 2, elemData.position.top + elemData.dimension.height / 2]
        let finalCenter = rotatePoint(newCenter, oldCenter, -elemData.transform.angle)
        elemData.position.left = finalCenter.x - elemData.dimension.width / 2
        elemData.position.top = finalCenter.y - elemData.dimension.height / 2

        ctx.restore()
        insertBlock(elemData)
        ctx.save()


        let left = elemData.position.left + elemData.dimension.width / 2
        let top = elemData.position.top + (elemData.dimension.height / 2)

        // // Rotate the canvas by 45 degrees
        ctx.font = `${elemData.font.italic ? 'italic ' : ''}${elemData.font.bold ? 'bold ' : ''}${elemData.font.size}px ${elemData.font.family}`;
        ctx.fillStyle = `${elemData.font.color}`;
        // now generate the text over it

        let angle = elemData.transform.angle
        ctx.translate(left, top)
        ctx.rotate(angle * Math.PI / 180);
        let horizontalPadding = elemData.font.size / 5
        let verticalPadding = elemData.font.size
        for (let i = 0; i < linesArr.length; i++) {
            ctx.fillText(
                linesArr[i][0],
                linesArr[i][1] - elemData.dimension.width / 2 + horizontalPadding,
                linesArr[i][2] - elemData.dimension.height / 2 + verticalPadding
            )
        }

        ctx.restore()

    }

    function insertBlock(elemData) {
        elemData.points = []
        let centerPoint = [elemData.position.left + elemData.dimension.width / 2, elemData.position.top + elemData.dimension.height / 2]
        elemData.points.push(rotatePoint([elemData.position.left, elemData.position.top], centerPoint, -elemData.transform.angle))
        elemData.points.push(rotatePoint([elemData.position.left + elemData.dimension.width, elemData.position.top], centerPoint, -elemData.transform.angle))
        elemData.points.push(rotatePoint([elemData.position.left + elemData.dimension.width, elemData.position.top + elemData.dimension.height], centerPoint, -elemData.transform.angle))
        elemData.points.push(rotatePoint([elemData.position.left, elemData.position.top + elemData.dimension.height], centerPoint, -elemData.transform.angle))
        const ctx = canvas.getContext('2d');
        ctx.save()
        let width = elemData.dimension.width
        let height = elemData.dimension.height
        let x = -elemData.dimension.width / 2;
        let y = -elemData.dimension.height / 2;
        let cornerRadius = elemData?.format?.borderRadius ? elemData.format.borderRadius : 0
        let radians = elemData.transform.angle * Math.PI / 180
        ctx.translate(elemData.position.left + elemData.dimension.width / 2, elemData.position.top + elemData.dimension.height / 2)
        ctx.rotate(radians)
        if (elemData.format.borderWidth) {
            ctx.lineWidth = elemData.format.borderWidth;
            ctx.strokeStyle = elemData.format.borderColor;
        }
        ctx.beginPath();
        ctx.moveTo(x + cornerRadius, y);
        ctx.arcTo(x + width, y, x + width, y + height, cornerRadius);
        ctx.arcTo(x + width, y + height, x, y + height, cornerRadius);
        ctx.arcTo(x, y + height, x, y, cornerRadius);
        ctx.arcTo(x, y, x + width, y, cornerRadius);
        ctx.closePath();
        ctx.fillStyle = elemData.format.backgroundColor
        ctx.fill();
        if (elemData.format.borderWidth) ctx.stroke();

        ctx.restore()
    }

    function renderElement(elemData) {
        if (elemData.type == 'text') {
            insertText(elemData)
        }
        else if (elemData.type == 'block') {
            insertBlock(elemData)
        }
        else if (elemData.type == 'overlay') {
            insertOverlay(elemData)
        }
        else if (elemData.type == 'image') {
            insertImage(elemData)
        }
    }

    function insertHandles(elemData) {
        insertBlock(elemData)

        let ctx = canvas.getContext('2d')
        ctx.save()
        let centerPoint = [elemData.position.left + elemData.dimension.width / 2, elemData.position.top + elemData.dimension.height / 2]
        let rotateHandleMinHeight = 50

        ctx.lineWidth = elemData.format.borderWidth;
        ctx.strokeStyle = elemData.format.borderColor;
        ctx.beginPath();
        ctx.translate(centerPoint[0], centerPoint[1])
        ctx.rotate(elemData.transform.angle * Math.PI / 180)
        ctx.moveTo(0, -elemData.dimension.height / 2);
        ctx.lineTo(0, -rotateHandleMinHeight - elemData.dimension.height / 2);
        ctx.closePath();
        ctx.stroke();
        ctx.restore()


        ctx = canvas.getContext('2d')
        ctx.save()
        ctx.beginPath();
        ctx.lineWidth = constants.handleLineWidth;
        ctx.strokeStyle = constants.handleStrokeStyle;
        ctx.fillStyle = constants.fillStyle
        ctx.translate(elemData.position.left + elemData.dimension.width / 2, elemData.position.top + elemData.dimension.height / 2)
        ctx.rotate(elemData.transform.angle * Math.PI / 180)

        function drawHandle(handleIndex, handleName, point) {
            ctx.moveTo(startPos.x, startPos.y);
            ctx.lineTo(startPos.x + constants.handleWidth, startPos.y);
            ctx.lineTo(startPos.x + constants.handleWidth, startPos.y + constants.handleWidth);
            ctx.lineTo(startPos.x, startPos.y + constants.handleWidth);
            ctx.lineTo(startPos.x, startPos.y - constants.handleLineWidth / 2);
            if (point) {
                let points = [
                    rotatePoint([point.x - constants.handleWidth / 2, point.y - constants.handleWidth / 2], centerPoint, -elemData.transform.angle),
                    rotatePoint([point.x + constants.handleWidth / 2, point.y - constants.handleWidth / 2], centerPoint, -elemData.transform.angle),
                    rotatePoint([point.x + constants.handleWidth / 2, point.y + constants.handleWidth / 2], centerPoint, -elemData.transform.angle),
                    rotatePoint([point.x - constants.handleWidth / 2, point.y + constants.handleWidth / 2], centerPoint, -elemData.transform.angle),
                ]
                let basePoint = rotatePoint([point.x, point.y], centerPoint, -elemData.transform.angle)
                handleElements[handleIndex] = {
                    name: handleName,
                    ...handleElements[handleIndex], points,
                    position: { left: basePoint.x, top: basePoint.y }
                }
            }
        }

        // left middle handle
        let startPos = { x: -(elemData.dimension.width / 2) - constants.handleWidth / 2, y: -constants.handleWidth / 2 }
        drawHandle(0, 'left-middle', { x: elemData.position.left, y: elemData.position.top + elemData.dimension.height / 2 })
        // right middle handle
        startPos = { x: (elemData.dimension.width / 2) - constants.handleWidth / 2, y: -constants.handleWidth / 2 }
        drawHandle(1, 'right-middle', { x: elemData.position.left + elemData.dimension.width, y: elemData.position.top + elemData.dimension.height / 2 })
        // rotate handle
        startPos = { x: -constants.handleWidth / 2, y: -rotateHandleMinHeight + (-elemData.dimension.height - constants.handleWidth) / 2 }
        drawHandle(2, 'rotate', { x: elemData.position.left + elemData.dimension.width / 2, y: elemData.position.top - rotateHandleMinHeight })

        if (!(elemData.type == 'text')) {
            // top middle handle
            startPos = { x: - constants.handleWidth / 2, y: -(constants.handleWidth + elemData.dimension.height) / 2 }
            drawHandle(3, 'top-middle', { x: elemData.position.left + elemData.dimension.width / 2, y: elemData.position.top })
            // bottom middle handle
            startPos = { x: - constants.handleWidth / 2, y: (elemData.dimension.height - constants.handleWidth) / 2 }
            drawHandle(4, 'bottom-middle', { x: elemData.position.left + elemData.dimension.width / 2, y: elemData.position.top + elemData.dimension.height })
            // // top left handle
            // startPos = { x: - (constants.handleWidth + elemData.dimension.width) / 2, y: -(elemData.dimension.height + constants.handleWidth) / 2 }
            // drawHandle(5, 'top-left', { x: elemData.position.left, y: elemData.position.top })
            // // top right handle
            // startPos = { x: (elemData.dimension.width - constants.handleWidth) / 2, y: -(elemData.dimension.height + constants.handleWidth) / 2 }
            // drawHandle(6, 'top-right', { x: elemData.position.left + elemData.dimension.width, y: elemData.position.top })
            // // bottom left handle
            // startPos = { x: (-elemData.dimension.width - constants.handleWidth) / 2, y: (elemData.dimension.height - constants.handleWidth) / 2 }
            // drawHandle(7, 'bottom-left', { x: elemData.position.left, y: elemData.position.top + elemData.dimension.height })
            // // bottom right handle
            // startPos = { x: (elemData.dimension.width - constants.handleWidth) / 2, y: (elemData.dimension.height - constants.handleWidth) / 2 }
            // drawHandle(8, 'bottom-right', { x: elemData.position.left + elemData.dimension.width, y: elemData.position.top + elemData.dimension.height })
        }
        ctx.stroke()
        ctx.fill()
        ctx.restore()
    }

    function insertOverlay(elemData) {
        let ctx = canvas.getContext('2d')
        ctx.save()

        elemData.points = []
        let centerPoint = [elemData.position.left + elemData.dimension.width / 2, elemData.position.top + elemData.dimension.height / 2]
        elemData.points.push(rotatePoint([elemData.position.left, elemData.position.top], centerPoint, -elemData.transform.angle))
        elemData.points.push(rotatePoint([elemData.position.left + elemData.dimension.width, elemData.position.top], centerPoint, -elemData.transform.angle))
        elemData.points.push(rotatePoint([elemData.position.left + elemData.dimension.width, elemData.position.top + elemData.dimension.height], centerPoint, -elemData.transform.angle))
        elemData.points.push(rotatePoint([elemData.position.left, elemData.position.top + elemData.dimension.height], centerPoint, -elemData.transform.angle))
        let width = elemData.dimension.width
        let height = elemData.dimension.height
        let x = -elemData.dimension.width / 2;
        let y = -elemData.dimension.height / 2;
        let cornerRadius = elemData?.format?.borderRadius ? elemData.format.borderRadius : 0
        let radians = elemData.transform.angle * Math.PI / 180
        ctx.translate(elemData.position.left + elemData.dimension.width / 2, elemData.position.top + elemData.dimension.height / 2)
        ctx.rotate(radians)

        // remove the rectangular area
        ctx.beginPath();
        ctx.globalCompositeOperation = 'destination-out'
        ctx.moveTo(x + cornerRadius, y);
        ctx.arcTo(x + width, y, x + width, y + height, cornerRadius);
        ctx.arcTo(x + width, y + height, x, y + height, cornerRadius);
        ctx.arcTo(x, y + height, x, y, cornerRadius);
        ctx.arcTo(x, y, x + width, y, cornerRadius);
        ctx.closePath();
        ctx.fillStyle = "white"
        ctx.fill();

        // add The border
        ctx.beginPath();
        ctx.globalCompositeOperation = 'source-over'
        ctx.moveTo(x + cornerRadius, y);
        ctx.arcTo(x + width, y, x + width, y + height, cornerRadius);
        ctx.arcTo(x + width, y + height, x, y + height, cornerRadius);
        ctx.arcTo(x, y + height, x, y, cornerRadius);
        ctx.arcTo(x, y, x + width, y, cornerRadius);
        ctx.closePath();
        ctx.fillStyle = "#00000000"
        ctx.fill();

        if (elemData.format.borderWidth) {
            ctx.lineWidth = elemData.format.borderWidth;
            ctx.strokeStyle = elemData.format.borderColor;
        }
        ctx.stroke();


        ctx.restore()
    }

    function insertImage(elemData) {

        // to background and border if required
        let tempObj = JSON.parse(JSON.stringify(elemData))
        insertBlock(tempObj)

        if (elemData.image) {
            if (elemData.image.id && !elemData.image.loading) {
                elemData.image.loading = false
                let imageObj = images.find(item => item.id == elemData.image.id)
                if (imageObj?.el) {

                    let ctx = canvas.getContext('2d')
                    ctx.save()

                    elemData.points = []
                    let centerPoint = [elemData.position.left + elemData.dimension.width / 2, elemData.position.top + elemData.dimension.height / 2]
                    elemData.points.push(rotatePoint([elemData.position.left, elemData.position.top], centerPoint, -elemData.transform.angle))
                    elemData.points.push(rotatePoint([elemData.position.left + elemData.dimension.width, elemData.position.top], centerPoint, -elemData.transform.angle))
                    elemData.points.push(rotatePoint([elemData.position.left + elemData.dimension.width, elemData.position.top + elemData.dimension.height], centerPoint, -elemData.transform.angle))
                    elemData.points.push(rotatePoint([elemData.position.left, elemData.position.top + elemData.dimension.height], centerPoint, -elemData.transform.angle))
                    let width = elemData.dimension.width
                    let height = elemData.dimension.height
                    let x = -elemData.dimension.width / 2;
                    let y = -elemData.dimension.height / 2;
                    let cornerRadius = elemData?.format?.borderRadius ? elemData.format.borderRadius : 0
                    let radians = elemData.transform.angle * Math.PI / 180
                    ctx.translate(elemData.position.left + elemData.dimension.width / 2, elemData.position.top + elemData.dimension.height / 2)
                    ctx.rotate(radians)

                    ctx.scale((elemData.transform.xFlip ? -1 : 1), (elemData.transform.yFlip ? -1 : 1))
                    ctx.drawImage(imageObj.el, x, y, width, height)

                    ctx.restore()
                } else {
                    loadImage(elemData)
                }

            } else if (!elemData.image.loading) {
                loadImage(elemData)
            }

        }

    }


    function loadImage(elemData) {
        ++assetsToLoad
        elemData.image.loading = true
        let img = null;
        if (isModule) img = new cImage
        else img = new Image
        img.onload = function () {
            elemData.image.id = `img-${uniqueCounter++}`
            images.push({
                id: elemData.image.id,
                el: img,
            })
            elemData.image.loading = false
            --assetsToLoad
            reRenderCanvas()
        }
        img.onerror = err => { throw err }
        img.src = elemData.image.src
    }

    function modifySelected(obj, option = { isHistory: false, toRender: true }) {
        let historyObj = {}
        let elementToModify = option.isHistory ? obj.selectedElement : selectedElement
        if (elementToModify && obj) {
            historyObj.selectedElement = JSON.parse( JSON.stringify(elementToModify) )
            elementToModify = elements.find( item => item.id==elementToModify.id )
            if (obj.hasOwnProperty('text')) {
                elementToModify.text = obj.text
                historyObj.text = elementToModify.text
            }
            if (obj?.font) {
                historyObj.font = {}
                if (obj.font.hasOwnProperty('color')) {
                    elementToModify.font.color = obj.font.color
                    historyObj.font.color = elementToModify.font.color
                }
                if (obj.font.hasOwnProperty('size')) {
                    elementToModify.font.size = obj?.font?.size
                    historyObj.font.size = elementToModify.font.size
                }
                if (obj.font.hasOwnProperty('family')) {
                    elementToModify.font.family = obj?.font?.family
                    historyObj.font.family = elementToModify.font.family
                }
                if (obj.font.hasOwnProperty('familyUrl')) {
                    elementToModify.font.familyUrl = obj?.font
                    historyObj.font.familyUrl = elementToModify.font.familyUrl
                }
                if (obj.font.hasOwnProperty('bold')) {
                    elementToModify.font.bold = obj?.font?.bold ? true : false
                    historyObj.font.bold = elementToModify.font.bold
                }
                if (obj.font.hasOwnProperty('italic')) {
                    elementToModify.font.italic = obj?.font?.italic ? true : false
                    historyObj.font.italic = elementToModify.font.italic
                }
            }
            if (obj?.position) {
                historyObj.position = {}
                if (obj.position.hasOwnProperty('top')) {
                    elementToModify.position.top = obj.position.top
                    historyObj.position.top = elementToModify.position.top
                }
                if (obj.position.hasOwnProperty('left')) {
                    elementToModify.position.left = obj.position.left
                    historyObj.position.left = elementToModify.position.left
                }
            }
            if (obj?.dimension) {
                historyObj.dimension = {}
                if (obj.dimension.hasOwnProperty('width')) {
                    elementToModify.dimension.width = obj.dimension.width
                    historyObj.dimension.width = elementToModify.dimension.width
                }
                if (obj.dimension.hasOwnProperty('height')) {
                    elementToModify.dimension.height = obj.dimension.height
                    historyObj.dimension.height = elementToModify.dimension.height
                }
            }
            if (obj?.format) {
                historyObj.format = {}
                if (obj.format.hasOwnProperty('borderColor')) {
                    elementToModify.format.borderColor = obj.format.borderColor
                    historyObj.format.borderColor = elementToModify.format.borderColor
                }
                if (obj.format.hasOwnProperty('backgroundColor')) {
                    elementToModify.format.backgroundColor = obj.format.backgroundColor
                    historyObj.format.backgroundColor = elementToModify.format.backgroundColor
                }
                if (obj.format.hasOwnProperty('borderWidth')) {
                    elementToModify.format.borderWidth = obj.format.borderWidth
                    historyObj.format.borderWidth = elementToModify.format.borderWidth
                }
            }
            if (obj?.transform) {
                historyObj.transform = {}
                if (obj.transform.hasOwnProperty('xFlip')) {
                    elementToModify.transform.xFlip = obj.transform.xFlip
                    historyObj.transform.xFlip = elementToModify.transform.xFlip
                }
                if (obj.transform.hasOwnProperty('yFlip')) {
                    elementToModify.transform.yFlip = obj.transform.yFlip
                    historyObj.transform.yFlip = elementToModify.transform.yFlip
                }
                if (obj.transform.hasOwnProperty('angle')) {
                    elementToModify.transform.angle = obj.transform.angle
                    historyObj.transform.angle = elementToModify.transform.angle
                }
            }
        }
        if (!option.isHistory) {
            history.push(historyObj)
            historyIndex = history.length - 1
        }
        if (option.toRender) reRenderCanvas()
        window.historyList = history
    }
    function undo() {
        if (historyIndex < 0) {
            if (configuration.isDev) console.warn('UNDO: no previous history state to go to')
            return
        }
        let thisData = history[historyIndex]
        if (thisData?.selectedElement) {
            let selectedElement = elements.find(item => item.id == thisData.selectedElement.id)
            thisData.selectedElement.selectedElement = JSON.parse(JSON.stringify(selectedElement))
            modifySelected(thisData.selectedElement, { toRender: true, isHistory: true })
            --historyIndex
        }
    }
    function redo() {
        if (historyIndex>=history.length-1){
            if (configuration.isDev) console.warn('REDO: no next history state to go to')
            return
        }
        if (historyIndex < history.length) {
            let thisData = history[historyIndex+1]
            let selectedElement = elements.find(item => item.id == thisData.selectedElement.id)
            thisData.selectedElement = JSON.parse( JSON.stringify(selectedElement) )
            modifySelected(thisData, { isHistory: true, toRender: true })
            ++historyIndex
        }
    }
    function clearHistory() {
        // this is called when you want to clear the undo/redo states
        // you won't be able to undo or redo if yuou invoke this method.

        initialData = JSON.parse(JSON.stringify(elements))
        history = []

    }

    let returnObj = {
        getCanvas() { return canvas },
        getElements() { return JSON.parse(JSON.stringify(elements)) },
        getHandles() { return JSON.parse(JSON.stringify(handleElements)) },
        mount(data) {
            configuration = { ...configuration, ...data }
            if (!isModule) container = document.getElementById(data.containerId)
            init(data)
        },
        addElement(elemData) {
            if (!elemData.id) elemData.id = `elem-${new Date().getTime()}-${uniqueCounter++}`
            elements.push(elemData)
            reRenderCanvas()
        },
        clearCanvas() {
            elements = []
            selectedElement = null
            selectedHandle = null
            reRenderCanvas()
        },
        reRenderCanvas,
        toCanvasImage() {
            if (!isModule) {
                var image = new Image();
                image.src = canvas.toDataURL("image/png");
                var win = window.open();
                win.document.write(image.outerHTML);
            }
        },
        getSelectedItemData() {
            if (selectedElement) return JSON.parse(JSON.stringify(selectedElement))
            return null
        },
        modifySelected,
        deleteElement(id) {
            if (id) {
                let elementIndex = elements.findIndex(item => item.id == id)
                if (elementIndex != -1) elements.splice(elementIndex, 1)
            }
            selectedElement = null
            selectedHandle = null
            isAction = null
            actionData.actionType = null
            mouseUp()
            reRenderCanvas()
        },
        async getBuffer() {

            return new Promise((res, rej) => {
                callbackAfterLoading = () => {
                    if (isModule) {
                        let newOptions = ['image/png', { compressionLevel: 3, filters: canvas.PNG_FILTER_NONE }]
                        res(canvas.toBuffer(...newOptions))
                    }
                    else {
                        var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                        res(imageData.data);
                    }
                }
                reRenderCanvas()
            })
        },
        undo, redo, clearHistory,
    }
    return returnObj
}

if (isModule) module.exports = CanvasEditor