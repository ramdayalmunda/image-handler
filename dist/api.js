
const api = {
    post: function( url, body, headers, method='POST' ){
        let finalUrl = ""
        let origin = window.origin+"/api"
        url = url.trim()
        if (url[0]=='/') finalUrl = `${origin}${url}`
        else if (url.slice(0,4)=='http') finalUrl = url
        else finalUrl = `${origin}/${url}`
        return new Promise( async (res, rej)=>{
            fetch( finalUrl, {
                method,
                headers: {
                  'Content-Type': 'application/json',
                  ...headers
                },
                body: JSON.stringify({ ...body }),
            } ).then( async response=>{
                response.data = await response.json()
                res(response)
            } )
        } )
    }
}

export default api